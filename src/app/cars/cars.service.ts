import { Injectable } from '@angular/core';
import {Observable} from "rxjs/index";
import {Car} from "../shared/models/car";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  constructor(private http: HttpClient) { }

  getByUserId(userId: string): Observable<any> {
    return this.http.get(`/api/users/${userId}/cars`);
  }

  addCar(userId: string, car: Car): Observable<any> {
    return this.http.post(`/api/users/${userId}/cars`, car);
  }

  updateCar(userId: string, car: Car): Observable<any> {
    return this.http.put(`/api/users/${userId}/cars/${car.id}`, car);
  }

  deleteCar(userId: string, carId: string): Observable<any> {
    return this.http.delete(`/api/users/${userId}/cars/${carId}`);
  }
}
