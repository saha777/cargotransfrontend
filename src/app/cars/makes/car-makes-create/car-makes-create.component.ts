import { Component, OnInit } from '@angular/core';
import {CarMakesService} from '../cars-makes.service';
import {CarMakes} from '../../../shared/models/car-makes';
import {Router} from '@angular/router';

@Component({
  selector: 'app-car-makes-create',
  templateUrl: './car-makes-create.component.html',
  styleUrls: ['./car-makes-create.component.css']
})
export class CarMakesCreateComponent implements OnInit {

  newCarMakes: CarMakes;

  constructor(private carsMakesService: CarMakesService,
              private router: Router) { }

  ngOnInit() {
    this.newCarMakes = new CarMakes();
  }

  submit() {
    this.carsMakesService.add(this.newCarMakes).subscribe(
      contactData => {
        this.router.navigate(['/cars/makes']);
      }, error2 => {
        console.error(error2);
      }
    );
  }
}
