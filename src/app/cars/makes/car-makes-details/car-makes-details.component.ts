import { Component, OnInit } from '@angular/core';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../cars-makes.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-car-makes-details',
  templateUrl: './car-makes-details.component.html',
  styleUrls: ['./car-makes-details.component.css']
})
export class CarMakesDetailsComponent implements OnInit {

  carMakes: CarMakes = new CarMakes();

  constructor(private carsMakesService: CarMakesService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadCarMakes();
  }

  loadCarMakes() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.carsMakesService.getByMakeId(id).subscribe(
        data => {
          this.carMakes = data;
        }, error2 => {
          console.error(error2);
        }
      );
    });
  }

}
