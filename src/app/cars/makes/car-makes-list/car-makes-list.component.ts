import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../shared/models/user';
import {Car} from '../../../shared/models/car';
import {CarsService} from '../../cars.service';
import {isNull, isUndefined} from 'util';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../cars-makes.service';

@Component({
  selector: 'app-car-makes-list',
  templateUrl: './car-makes-list.component.html',
  styleUrls: ['./car-makes-list.component.css']
})
export class CarMakesListComponent implements OnInit {
  carMakes: CarMakes[] = [];

  constructor(private carsMakesService: CarMakesService) { }

  ngOnInit() {
    this.loadCarMakes();
  }

  loadCarMakes() {
    this.carsMakesService.getAll().subscribe(
      data => {
        this.carMakes = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  submitDeletedCar(i: number) {
    this.carsMakesService.delete(this.carMakes[i].id).subscribe(
      data => {
        this.carMakes.splice(i, 1);
      }, error2 => {
        console.error(error2);
      }
    );
  }

}
