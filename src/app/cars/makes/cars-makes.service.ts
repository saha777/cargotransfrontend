import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CarMakes} from '../../shared/models/car-makes';

@Injectable({
  providedIn: 'root'
})
export class CarMakesService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {
    return this.http.get(`/api/cars/makes`);
  }

  public getByMakeId(makeId: number): Observable<any> {
    return this.http.get(`/api/cars/makes/${makeId}`);
  }

  public add(carsMakes: CarMakes): Observable<any> {
    return this.http.post(`/api/cars/makes/`, carsMakes);
  }

  public update(carsMakes: CarMakes): Observable<any> {
    return this.http.put(`/api/cars/makes/`, carsMakes);
  }

  public delete(makeId: number): Observable<any> {
    return this.http.delete(`/api/cars/makes/${makeId}`);
  }
}
