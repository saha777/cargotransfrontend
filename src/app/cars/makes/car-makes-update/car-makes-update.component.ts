import { Component, OnInit } from '@angular/core';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../cars-makes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GeocodeService} from '../../../shared/services/geocode-service.service';

@Component({
  selector: 'app-car-makes-update',
  templateUrl: './car-makes-update.component.html',
  styleUrls: ['./car-makes-update.component.css']
})
export class CarMakesUpdateComponent implements OnInit {

  carMakes: CarMakes = new CarMakes();

  constructor(private carsMakesService: CarMakesService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadCarMakes();
  }

  loadCarMakes() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.carsMakesService.getByMakeId(id).subscribe(
        data => {
          this.carMakes = data;
        }, error2 => {
          console.error(error2);
        }
      );
    });
  }

  submit() {
    this.carsMakesService.update(this.carMakes).subscribe(
      contactData => {
        this.router.navigate(['/cars/makes']);
      }, error2 => {
        console.error(error2);
      }
    );
  }
}
