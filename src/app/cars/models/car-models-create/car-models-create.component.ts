import { Component, OnInit } from '@angular/core';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../../makes/cars-makes.service';
import {Router} from '@angular/router';
import {CarModel} from '../../../shared/models/car-model';
import {CarModelsService} from '../car-models.service';

@Component({
  selector: 'app-car-models-create',
  templateUrl: './car-models-create.component.html',
  styleUrls: ['./car-models-create.component.css']
})
export class CarModelsCreateComponent implements OnInit {

  newCarModel: CarModel;
  makes: CarMakes[];

  constructor(private carsMakesService: CarMakesService,
              private carsModelsService: CarModelsService,
              private router: Router) { }

  ngOnInit() {
    this.newCarModel = new CarModel();
    this.loadCarsMakes();
  }

  loadCarsMakes() {
    this.carsMakesService.getAll().subscribe(
      data => {
        this.makes = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  submit() {
    this.carsModelsService.add(this.newCarModel).subscribe(
      data => {
        this.router.navigate(['/cars/makes']);
      }, error2 => {
        console.error(error2);
      }
    );
  }
}
