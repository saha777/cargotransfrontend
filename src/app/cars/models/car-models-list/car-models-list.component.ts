import {Component, Input, OnInit} from '@angular/core';
import {CarModel} from '../../../shared/models/car-model';
import {CarMakesService} from '../../makes/cars-makes.service';
import {CarModelsService} from '../car-models.service';

@Component({
  selector: 'app-car-models-list',
  templateUrl: './car-models-list.component.html',
  styleUrls: ['./car-models-list.component.css']
})
export class CarModelsListComponent implements OnInit {

  @Input() models: CarModel[];

  constructor(private carsModelsService: CarModelsService) { }

  ngOnInit() {
  }

  submitDeletedCar(i: number) {
    this.carsModelsService.delete(this.models[i].id).subscribe(
      data => {
        this.models.splice(i, 1);
      }, error2 => {
        console.error(error2);
      }
    );
  }

}
