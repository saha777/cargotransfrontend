import { Component, OnInit } from '@angular/core';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../../makes/cars-makes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CarModel} from '../../../shared/models/car-model';
import {CarModelsService} from '../car-models.service';

@Component({
  selector: 'app-car-models-update',
  templateUrl: './car-models-update.component.html',
  styleUrls: ['./car-models-update.component.css']
})
export class CarModelsUpdateComponent implements OnInit {

  carModel: CarModel = new CarModel();
  makes: CarModel[];

  constructor(private carMakesService: CarMakesService,
              private carModelsService: CarModelsService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadCarsMakes();
    this.loadCarModel();
  }

  loadCarsMakes() {
    this.carMakesService.getAll().subscribe(
      data => {
        this.makes = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  loadCarModel() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.carModelsService.getById(id).subscribe(
        data => {
          this.carModel = data;
        }, error2 => {
          console.error(error2);
        }
      );
    });
  }

  submit() {
    this.carModelsService.update(this.carModel).subscribe(
      contactData => {
        this.router.navigate(['/cars/makes']);
      }, error2 => {
        console.error(error2);
      }
    );
  }

}
