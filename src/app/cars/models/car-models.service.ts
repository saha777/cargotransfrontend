import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CarModel} from '../../shared/models/car-model';

@Injectable({
  providedIn: 'root'
})
export class CarModelsService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<any> {
    return this.http.get(`/api/cars/models`);
  }

  public getByMakeId(makeId: number): Observable<any> {
    return this.http.get(`/api/cars/models/makes/${makeId}`);
  }

  public getById(modelId: number): Observable<any> {
    return this.http.get(`/api/cars/models/${modelId}`);
  }

  public add(carsModels: CarModel): Observable<any> {
    return this.http.post(`/api/cars/models/`, carsModels);
  }

  public update(carsModels: CarModel): Observable<any> {
    return this.http.put(`/api/cars/models/`, carsModels);
  }

  public delete(modelId: number): Observable<any> {
    return this.http.delete(`/api/cars/models/${modelId}`);
  }
}
