import { Component, OnInit } from '@angular/core';
import {CarMakes} from '../../../shared/models/car-makes';
import {CarMakesService} from '../../makes/cars-makes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CarModel} from '../../../shared/models/car-model';
import {CarModelsService} from '../car-models.service';

@Component({
  selector: 'app-car-models-details',
  templateUrl: './car-models-details.component.html',
  styleUrls: ['./car-models-details.component.css']
})
export class CarModelsDetailsComponent implements OnInit {

  carModel: CarModel = new CarModel();

  constructor(private carsModelsService: CarModelsService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadCarMakes();
  }

  loadCarMakes() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.carsModelsService.getById(id).subscribe(
        data => {
          this.carModel = data;
        }, error2 => {
          console.error(error2);
        }
      );
    });
  }

}
