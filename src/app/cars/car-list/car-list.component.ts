import {Component, Input, OnInit} from '@angular/core';
import {UsersService} from '../../users/users.service';
import {Car} from '../../shared/models/car';
import {isUndefined, isNull} from 'util';
import {CarsService} from '../cars.service';
import {User} from '../../shared/models/user';
import {CarMakes} from '../../shared/models/car-makes';
import {CarMakesService} from '../makes/cars-makes.service';
import {CarModelsService} from '../models/car-models.service';
import {CarModel} from '../../shared/models/car-model';
import index from '@angular/cli/lib/cli';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {
  @Input() driver: User;
  cars: Car[] = [];
  newCar: Car;
  todayDate: Date;
  todayNumber: number;

  makeses: CarMakes[];
  models: CarModel[];

  constructor(private carMakesService: CarMakesService,
              private carModelsService: CarModelsService,
              private carsService: CarsService) { }

  ngOnInit() {
    this.loadDriverCars();
    this.loadMakes();
    this.initDates();
    this.initCar();
  }

  loadDriverCars() {
    this.carsService.getByUserId(this.driver.id).subscribe(
      data => {

        this.cars = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  loadMakes() {
    this.carMakesService.getAll().subscribe(
      data => {
        this.makeses = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  loadModels() {
    let id = 0;
    for (const makes of this.makeses) {
      if (makes.name === this.newCar.makes) {
        id = makes.id;
      }
    }

    this.carModelsService.getByMakeId(id).subscribe(
      data => {
        this.models = data;
      }, error2 => {
        console.error(error2);
      }
    );
  }

  initDates() {
    this.todayDate = this.getTodayString();
    this.todayNumber = this.todayDate.getTime();
  }

  private getTodayString(): Date {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth();
    const yyyy = today.getFullYear();
    return new Date(yyyy, mm, dd);
  }

  initCar() {
    this.newCar = new Car();
  }

  isInvalidVolume(): boolean {
    return this.newCar.maxCargoVolume < 1;
  }

  isInvalidMass(): boolean {
    return this.newCar.maxCargoMass < 1;
  }

  isIssueDate(): boolean {
    console.log();
    if (isUndefined(this.newCar.issueDate) || isNull(this.newCar.issueDate)) {
      return false;
    }
    return Date.now() < this.newCar.issueDate.getTime();
  }

  setIssueDate(value) {
    const newDate = new Date(value);
    this.newCar.issueDate = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());
  }

  submitCar() {
    this.carsService.addCar(this.driver.id, this.newCar).subscribe(
      data => {
        this.cars.push(data);
        this.newCar = new Car();
      }, error2 => {
        console.error(error2);
      }
    );
  }

  submitDeletedCar(i: number) {
    this.carsService.deleteCar(this.driver.id, this.cars[i].id).subscribe(
      data => {
        this.cars.splice(i, 1);
      }, error2 => {
        console.error(error2);
      }
    );
  }

}
