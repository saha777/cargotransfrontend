import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {CreateAdvertisementComponent} from './advertisements/create-advertisement/create-advertisement.component';
import {AppRoutingModule} from './app-routing.module';
import {AdvertisementsService} from './advertisements/advertisements.service';
import {FormsModule} from '@angular/forms';
import { AdvertisementsComponent } from './advertisements/advertisements/advertisements.component';
import { AdCardComponent } from './advertisements/ad-card/ad-card.component';
import {UsersService} from './users/users.service';
import {AgmCoreModule} from '@agm/core';
import {RouterModule} from '@angular/router';
import {RequestsService} from './users/requests.service';
import { UserPageComponent } from './users/user-page/user-page.component';
import { LoginComponent } from './auth/login/login.component';
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider,
  SocialLoginModule,
  VkontakteLoginProvider
} from 'angular-6-social-login-v2';
import {AdvertisementPageComponent} from './advertisements/advertisement-page/advertisement-page.component';
import {GeocodeService} from './shared/services/geocode-service.service';
import {AccountService} from './shared/services/account.service';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { RegistrationComponent } from './auth/registration/registration.component';
import { ContactListComponent } from './contacts/contact-list/contact-list.component';
import {ModalWindow} from './shared/modal.window/modal.window.component';
import {TokenInterceptService} from './shared/services/token-intercept.service';
import {AppAuthService} from './shared/services/app-auth.service';
import {AuthGuard} from './shared/services/auth-guard.service';
import { CarListComponent } from './cars/car-list/car-list.component';
import { AdListComponent } from './advertisements/ad-list/ad-list.component';
import { RequestsListComponent } from './requests/requests-list/requests-list.component';
import { CarMakesListComponent } from './cars/makes/car-makes-list/car-makes-list.component';
import { CarMakesCreateComponent } from './cars/makes/car-makes-create/car-makes-create.component';
import { CarMakesUpdateComponent } from './cars/makes/car-makes-update/car-makes-update.component';
import { CarMakesDetailsComponent } from './cars/makes/car-makes-details/car-makes-details.component';
import { CarModelsListComponent } from './cars/models/car-models-list/car-models-list.component';
import { CarModelsCreateComponent } from './cars/models/car-models-create/car-models-create.component';
import { CarModelsUpdateComponent } from './cars/models/car-models-update/car-models-update.component';
import { CarModelsDetailsComponent } from './cars/models/car-models-details/car-models-details.component';
import {AdminRules} from './shared/services/admin-rules.service';
import {CarModelsService} from './cars/models/car-models.service';
import { UpdateAdComponent } from './advertisements/update-ad/update-ad.component';

export function getAuthServiceConfigs() {
  return new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('Your-Facebook-app-id')
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        // provider: new GoogleLoginProvider('Your-Google-app-id')
        provider: new GoogleLoginProvider('73649798274-j5esh6pku0bnqf7936a3ttn52057mjev.apps.googleusercontent.com')
      },
      {
        id: VkontakteLoginProvider.PROVIDER_ID,
        provider: new VkontakteLoginProvider('Your-VK-Client-id')
      },
      {
        id: LinkedinLoginProvider.PROVIDER_ID,
        provider: new LinkedinLoginProvider('1098828800522-m2ig6bieilc3tpqvmlcpdvrpvn86q4ks.apps.googleusercontent.com')
      },
    ]);
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CreateAdvertisementComponent,
    AdvertisementsComponent,
    AdCardComponent,
    UserPageComponent,
    LoginComponent,
    AdvertisementPageComponent,
    RegistrationComponent,
    ContactListComponent,
    ModalWindow,
    CarListComponent,
    AdListComponent,
    RequestsListComponent,
    CarMakesListComponent,
    CarMakesCreateComponent,
    CarMakesUpdateComponent,
    CarMakesDetailsComponent,
    CarModelsListComponent,
    CarModelsCreateComponent,
    CarModelsUpdateComponent,
    CarModelsDetailsComponent,
    UpdateAdComponent
  ],
  imports: [
    RouterModule,
    BrowserModule,
    FormsModule,
    SocialLoginModule,
    AppRoutingModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBbDIPcmPR_vF1NkVkzTk2q0jtfn8YYo6Y',
      libraries: ['geometry']
    })
  ],
  providers: [
    AuthGuard,
    AdminRules,
    AdvertisementsService,
    RequestsService,
    UsersService,
    AccountService,
    AppAuthService,
    {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
    GeocodeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
