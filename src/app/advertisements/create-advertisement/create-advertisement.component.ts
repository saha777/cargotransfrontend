import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AdvertisementsService} from '../advertisements.service';
import {Advertisement} from '../../shared/models/advertisement';
import {LocationDto} from '../../shared/models/location';
import {isNull, isUndefined} from 'util';
import {UsersService} from '../../users/users.service';
import {Router} from '@angular/router';
import {GeocodeService} from '../../shared/services/geocode-service.service';
import {AccountService} from '../../shared/services/account.service';

@Component({
  selector: 'app-create-advertisement',
  templateUrl: './create-advertisement.component.html',
  styleUrls: ['./create-advertisement.component.css']
})
export class CreateAdvertisementComponent implements OnInit {

  newAd: Advertisement = new Advertisement();
  todayNumber: number;
  todayDate: Date;

  // google maps zoom level
  zoom = 8;

  // initial center position for the map
  lat = 50.4439504;
  lng = 30.5040281;

  distance = -1;
  loading: boolean;

  constructor(private router: Router,
              private advertisementsService: AdvertisementsService,
              private accountService: AccountService,
              private geocodeService: GeocodeService) {
  }

  ngOnInit() {
    this.initDates();
    this.initLocations();
  }

  initDates() {
    this.todayDate = this.getTodayString();
    this.todayNumber = this.todayDate.getTime();
    this.newAd.orientedDateFrom = new Date();
    this.newAd.orientedDateTo = this.newAd.orientedDateFrom;
    this.newAd.cargoVolume = 1;
    console.log(this.todayDate);
    console.log(this.todayNumber);
  }

  initLocations() {
    this.newAd.startPoint = new LocationDto();
    this.newAd.startPoint.name = 'Irpin';
    this.newAd.startPoint.latitude = 50.51386322747108;
    this.newAd.startPoint.longitude = 30.221173066125516;
    this.newAd.startPoint.label = 'From';


    this.newAd.endPoint = new LocationDto();
    this.newAd.endPoint.name = 'Kiev';
    this.newAd.endPoint.latitude = 50.46580807938636;
    this.newAd.endPoint.longitude = 30.633427499283925;
    this.newAd.endPoint.label = 'To';

    this.updateDistance();
  }

  getTodayString(): Date {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth();
    const yyyy = today.getFullYear();
    return new Date(yyyy, mm, dd);
  }

  isInvalidOrientedDateFrom(): boolean {
    if (isUndefined(this.newAd.orientedDateFrom) || isNull(this.newAd.orientedDateFrom)) {
      return false;
    }
    return this.todayNumber > this.newAd.orientedDateFrom.getTime();
  }

  isInvalidOrientedDateTo(): boolean {
    if (isUndefined(this.newAd.orientedDateFrom) || isNull(this.newAd.orientedDateFrom)
      || isUndefined(this.newAd.orientedDateTo) || isNull(this.newAd.orientedDateTo)) {
      return false;
    }

    return this.newAd.orientedDateFrom.getTime() > this.newAd.orientedDateTo.getTime();
  }

  isInvalidVolume(): boolean {
    return this.newAd.cargoVolume < 1;
  }

  setOrientedDateFrom(value) {
    this.newAd.orientedDateFrom = new Date(value);
  }

  setOrientedDateTo(value) {
    this.newAd.orientedDateTo = new Date(value);
  }

  create() {
    this.newAd.owner = this.accountService.getUserAccount();
    this.newAd.ownerId = this.newAd.owner.id;
    this.advertisementsService.add(this.newAd).subscribe(
      data => {
        this.router.navigate(['/ads']);
      },
      error2 => {
        console.log(error2);
      }
    );
  }

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  searchCoordinates(isStart = true) {
    this.loading = true;
    this.geocodeService.geocodeAddress(isStart ? this.newAd.startPoint.name : this.newAd.endPoint.name)
      .subscribe(
        location => {
          if (isStart) {
            this.newAd.startPoint.latitude = location.lat;
            this.newAd.startPoint.longitude = location.lng;
          } else {
            this.newAd.endPoint.latitude = location.lat;
            this.newAd.endPoint.longitude = location.lng;
          }
          this.updateDistance();
          this.loading = false;
          // this.ref.detectChanges();
        }, error2 => {
          console.log(error2);
        }
      );
  }

  updateDistance() {
    this.distance = this.geocodeService.computeDistance(this.newAd.startPoint.latitude, this.newAd.startPoint.longitude, this.newAd.endPoint.latitude, this.newAd.endPoint.longitude);
    this.distance = (this.distance - (this.distance % 1)) / 1000;
  }


  mapClicked($event) {
    console.log($event);
    // getAddress($event.coords);
    // this.markers.push({
    //   lat: $event.coords.lat,
    //   lng: $event.coords.lng,
    //   draggable: true
    // });
  }

  onChooseLocation($event: MouseEvent) {
    console.log($event);
  }

  markerDragEnd(point: LocationDto, $event: {coords: {lat, lng}}) {
    console.log('dragEnd', $event);
    point.latitude = $event.coords.lat;
    point.longitude = $event.coords.lng;
    console.log('dragEnd', point);
  }

  // markers = [
  //   {
  //     lat: 50.673858,
  //     lng: 30.221173066125516,
  //     label: 'A',
  //     draggable: true
  //   },
  //   {
  //     lat: 51.373858,
  //     lng: 7.215982,
  //     label: 'B',
  //     draggable: false
  //   },
  //   {
  //     lat: 51.723858,
  //     lng: 7.895982,
  //     label: 'C',
  //     draggable: true
  //   }
  // ]
}
