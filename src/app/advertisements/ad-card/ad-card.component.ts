import {Component, Input, OnInit} from '@angular/core';
import {Advertisement} from "../../shared/models/advertisement";

@Component({
  selector: 'app-ad-card',
  templateUrl: './ad-card.component.html',
  styleUrls: ['./ad-card.component.css']
})
export class AdCardComponent implements OnInit {

  @Input('ad')
  ad: Advertisement;

  constructor() { }

  ngOnInit() {
  }

}
