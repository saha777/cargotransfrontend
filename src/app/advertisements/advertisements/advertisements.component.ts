import { Component, OnInit } from '@angular/core';
import {AdvertisementsService} from "../advertisements.service";
import {Advertisement} from "../../shared/models/advertisement";

@Component({
  selector: 'app-advertisements',
  templateUrl: './advertisements.component.html',
  styleUrls: ['./advertisements.component.css']
})
export class AdvertisementsComponent implements OnInit {

  ads: Advertisement[];

  constructor(private advertisementsService: AdvertisementsService) {
  }

  ngOnInit() {
    this.advertisementsService.getAll().subscribe(data => {
      console.log(data);
      this.ads = data;
    }, error2 => {
      if (error2.status === 200) {
        console.log(error2.error.text);
        this.ads = JSON.parse(error2.error.text);
      }
      console.log(error2);
    });
  }


}
