import { Component, OnInit } from '@angular/core';
import {GeocodeService} from '../../shared/services/geocode-service.service';
import {UsersService} from '../../users/users.service';
import {AdvertisementsService} from '../advertisements.service';
import {Advertisement} from '../../shared/models/advertisement';
import {Request} from '../../shared/models/request';
import {RequestsService} from '../../users/requests.service';
import {AccountService} from '../../shared/services/account.service';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../shared/models/user';
import {CarsService} from '../../cars/cars.service';
import {isUndefined} from 'util';
import {ContactsService} from '../../contacts/contacts.service';

@Component({
  selector: 'app-advertisement-page',
  templateUrl: './advertisement-page.component.html',
  styleUrls: ['./advertisement-page.component.css']
})
export class AdvertisementPageComponent implements OnInit {
  canCreateRequest = false;
  updateOperation = false;
  ad: Advertisement;
  user: User;
  distance = -1;
  newRequest: Request = new Request();

  constructor(private advertisementsService: AdvertisementsService,
              private usersService: UsersService,
              private carsService: CarsService,
              private contactsService: ContactsService,
              private accountService: AccountService,
              private requestsService: RequestsService,
              private geocodeService: GeocodeService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.user = this.accountService.getUserAccount();
    this.loadDriver();
    this.loadAdvertisement();
  }

  loadDriver() {
    console.log('this.user.isDriver :' + this.user.isDriver);
    if (this.user.isDriver) {
      console.log('callDriver :');
      this.loadCars();
    }
  }

  loadCars() {
    this.carsService.getByUserId(this.user.id).subscribe(
      data => {
        console.log(data);
        (this.user).cars = data;
      },
      error => {
        console.error(error);
      }
    );
  }

  loadOwner() {
    console.log('loadOwner');
    this.usersService.getById(this.ad.ownerId).subscribe(
      data => {
        console.log(data);
        this.ad.owner = data;
        this.loadRequests();
      },
      error => {
        console.error(error);
      }
    );
  }

  loadAdvertisement() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.advertisementsService.getById(id).subscribe(
        data => {
          console.log(data);
          this.ad = data;
          this.loadOwner();
          this.distance = this.geocodeService.computeDistance(
            this.ad.startPoint.latitude,
            this.ad.startPoint.longitude,
            this.ad.endPoint.latitude,
            this.ad.endPoint.longitude);
          this.distance = (this.distance - (this.distance % 1)) / 1000;
          if (this.ad.choosedRequest) {
            this.ad.choosedRequestId = this.ad.choosedRequest.id;
          }
        }, error2 => {
          console.log(error2);
        });
    });
  }

  loadRequests() {
    console.log('loadRequests');
    this.requestsService.getByAdId(this.ad.id).subscribe(
      data => {
        console.log(data);
        this.ad.requests = data;
        this.checkCreateRequest();
        this.loadRequesterContacts();
      },
      error => {
        console.error(error);
      }
    );
  }

  loadOwnerContacts() {
    console.log('loadOwnerContacts');
    if (this.ad.owner && this.ad.choosedRequest && this.ad.choosedRequest.driver.id === this.user.id) {
      this.contactsService.getByUserId(this.ad.owner.id).subscribe(
        contactsData => {
          console.log(contactsData);
          this.ad.owner.contacts = contactsData;
        }, error2 => {
          console.error(error2);
        }
      );
    }
  }

  loadRequesterContacts() {
    if (this.user.id === this.ad.owner.id) {
      for (const req of this.ad.requests) {
        this.contactsService.getByUserId(req.driver.id).subscribe(
          contactsData => {
            req.driver.contacts = contactsData;
          }, error2 => {
            console.error(error2);
          }
        );
      }
    }
  }

  submitRequest() {
    if (this.updateOperation) {
      this.requestsService.update(this.newRequest).subscribe(
        data => {
          this.updateOperation = false;
          this.loadRequests();
          this.checkCreateRequest();
        },
        error2 => {
          console.log(error2);
        }
      );
    } else {
      this.newRequest.driver = this.user;
      this.newRequest.advertisement = this.ad;
      console.log(this.newRequest);
      this.requestsService.add(this.newRequest).subscribe(
        data => {
          this.loadRequests();
          this.checkCreateRequest();
        },
        error2 => {
          console.log(error2);
        }
      );
    }
    this.newRequest = new Request();
  }

  updateReq(req: Request) {
    this.updateOperation = true;
    this.newRequest = req;
  }

  removeReq(req: Request) {
    this.requestsService.delete(req.id).subscribe(
      data => {
        this.loadRequests();
        this.checkCreateRequest();
      },
      error2 => {
        console.log(error2);
      }
    );
  }

  selectReq(req: Request) {
    this.advertisementsService.selectRequest(req).subscribe(
      data => {
        this.ad.choosedRequest = req;
        this.ad.choosedRequestId = req.id;
        this.ad.status = 'REQUEST_ACCEPTED';
      }, error2 => console.error(req)
    );
  }

  cancelReq(req: Request) {
    this.advertisementsService.cancelRequest(req).subscribe(
      data => {
        this.ad.choosedRequestId = null;
        this.ad.choosedRequest = null;
        this.ad.status = 'OPEN';
      }, error2 => console.error(error2)
    );
  }

  resolveAd() {
    const status = 'RESOLVED';
    this.advertisementsService.changeStatus(this.ad, status).subscribe(
      data => {
        this.ad.status = status;
      }, error2 => console.error(error2)
    );
  }

  cancelAd() {
    const status = 'CANCELED';
    this.advertisementsService.changeStatus(this.ad, status).subscribe(
      data => {
        this.ad.status = status;
      }, error2 => console.error(error2)
    );
  }

  checkCreateRequest() {
    this.canCreateRequest = true;
    for (const req of this.ad.requests) {
      if (req.driver.id === this.user.id) {
        this.canCreateRequest = false;
        if (this.ad.choosedRequestId === req.id) {
          this.ad.choosedRequest = req;
          this.loadOwnerContacts();
        }
        break;
      }
    }
  }

}
