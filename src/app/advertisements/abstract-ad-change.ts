import {Advertisement} from '../shared/models/advertisement';
import {Router} from '@angular/router';
import {AdvertisementsService} from './advertisements.service';
import {AccountService} from '../shared/services/account.service';
import {GeocodeService} from '../shared/services/geocode-service.service';
import {LocationDto} from '../shared/models/location';
import {isNull, isUndefined} from 'util';

export class AbstractAdChange {

  ad: Advertisement = new Advertisement();
  todayNumber: number;
  todayDate: Date;

  constructor(protected router: Router,
              protected advertisementsService: AdvertisementsService,
              protected accountService: AccountService,
              protected geocodeService: GeocodeService) {
  }

  protected initDates() {
    this.todayDate = this.getDateString();
    this.todayNumber = this.todayDate.getTime();
    this.ad.orientedDateFrom = new Date();
    this.ad.orientedDateTo = this.ad.orientedDateFrom;
    this.ad.cargoVolume = 1;
    console.log(this.todayDate);
    console.log(this.todayNumber);
  }

  protected updateDates() {
    this.ad.orientedDateFrom = this.getDateString(this.ad.orientedDateFrom);
    this.ad.orientedDateTo = this.getDateString(this.ad.orientedDateTo);
  }

  protected initLocations() {
    this.ad.startPoint = new LocationDto();
    this.ad.startPoint.name = 'Irpin';
    this.ad.startPoint.latitude = 50.51386322747108;
    this.ad.startPoint.longitude = 30.221173066125516;
    this.ad.startPoint.label = 'From';


    this.ad.endPoint = new LocationDto();
    this.ad.endPoint.name = 'Kiev';
    this.ad.endPoint.latitude = 50.46580807938636;
    this.ad.endPoint.longitude = 30.633427499283925;
    this.ad.endPoint.label = 'To';

    this.updateDistance();
  }

  protected getDateString(date: Date = new Date()): Date {
    const dd = date.getDate();
    const mm = date.getMonth();
    const yyyy = date.getFullYear();
    return new Date(yyyy, mm, dd);
  }

  protected isInvalidOrientedDateFrom(): boolean {
    if (isUndefined(this.ad.orientedDateFrom) || isNull(this.ad.orientedDateFrom)) {
      return false;
    }

    return this.todayDate > this.ad.orientedDateFrom;
  }

  protected isInvalidOrientedDateTo(): boolean {
    if (isUndefined(this.ad.orientedDateFrom) || isNull(this.ad.orientedDateFrom)
      || isUndefined(this.ad.orientedDateTo) || isNull(this.ad.orientedDateTo)) {
      return false;
    }

    return this.ad.orientedDateFrom > this.ad.orientedDateTo;
  }

  protected isInvalidVolume(): boolean {
    return this.ad.cargoVolume < 1;
  }

  protected setOrientedDateFrom(value) {
    this.ad.orientedDateFrom = new Date(value);
  }

  protected setOrientedDateTo(value) {
    this.ad.orientedDateTo = new Date(value);
  }
  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number = 50.4439504;
  lng: number = 30.5040281;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  distance = -1;
  loading: boolean;

  protected searchCoordinates(isStart = true) {
    this.loading = true;
    this.geocodeService.geocodeAddress(isStart ? this.ad.startPoint.name : this.ad.endPoint.name )
      .subscribe(
        location => {
          if (isStart) {
            this.ad.startPoint.latitude = location.lat;
            this.ad.startPoint.longitude = location.lng;
          } else {
            this.ad.endPoint.latitude = location.lat;
            this.ad.endPoint.longitude = location.lng;
          }
          this.updateDistance();
          this.loading = false;
          // this.ref.detectChanges();
        }, error2 => {
          console.log(error2);
        }
      );
  }

  protected updateDistance() {
    this.distance = this.geocodeService.computeDistance(this.ad.startPoint.latitude, this.ad.startPoint.longitude, this.ad.endPoint.latitude, this.ad.endPoint.longitude);
    this.distance = (this.distance - (this.distance % 1)) / 1000;
  }


  mapClicked($event) {
    console.log($event);
  }

  onChooseLocation($event: MouseEvent) {
    console.log($event);
  }

  markerDragEnd(point: LocationDto, $event: {coords: {lat, lng}}) {
    console.log('dragEnd', $event);
    point.latitude = $event.coords.lat;
    point.longitude = $event.coords.lng;
    console.log('dragEnd', point);
  }
}
