import {Component, OnInit} from '@angular/core';
import {AbstractAdChange} from '../abstract-ad-change';
import {ActivatedRoute, Router} from '@angular/router';
import {AdvertisementsService} from '../advertisements.service';
import {AccountService} from '../../shared/services/account.service';
import {GeocodeService} from '../../shared/services/geocode-service.service';

@Component({
  selector: 'app-update-ad',
  templateUrl: './update-ad.component.html',
  styleUrls: ['./update-ad.component.css']
})
export class UpdateAdComponent extends AbstractAdChange implements OnInit {
  private id = '';

  constructor(protected route: ActivatedRoute,
              protected router: Router,
              protected advertisementsService: AdvertisementsService,
              protected accountService: AccountService,
              protected geocodeService: GeocodeService) {
    super(router, advertisementsService, accountService, geocodeService);
  }

  ngOnInit() {
    this.initDates();
    this.initLocations();
    this.loadAd();
  }

  loadAd() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.advertisementsService.getById(this.id).subscribe(
        data => {
          console.log(data);
          this.ad = data;
          if (this.ad.ownerId !== this.accountService.getUserAccount().id) {
            this.redirect();
          }
          this.distance = this.geocodeService.computeDistance(
            this.ad.startPoint.latitude,
            this.ad.startPoint.longitude,
            this.ad.endPoint.latitude,
            this.ad.endPoint.longitude);
          this.distance = (this.distance - (this.distance % 1)) / 1000;
        }, error2 => {
          console.log(error2);
        });
    });
  }

  update() {
    this.ad.owner = this.accountService.getUserAccount();
    this.ad.ownerId = this.ad.owner.id;
    this.advertisementsService.update(this.ad).subscribe(
      data => {
        this.redirect();
      },
      error2 => {
        console.log(error2);
      }
    );
  }

  redirect() {
    this.router.navigate(['/ads/ad/' + this.id]);
  }

}
