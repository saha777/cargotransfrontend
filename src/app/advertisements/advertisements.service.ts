import {Injectable} from '@angular/core';
import {Advertisement} from '../shared/models/advertisement';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {Request} from '../shared/models/request';

@Injectable()
export class AdvertisementsService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<any> {
    return this.http.get(`/api/ads`);
  }

  public getById(id: string): Observable<any> {
    return this.http.get('/api/ads/' + id);
  }

  public getByUserId(id: string): Observable<any> {
    return this.http.get(`/api/ads/users/${id}`);
  }

  public add(ad: Advertisement): Observable<any> {
    return this.http.post(`/api/ads`, ad);
  }

  public update(ad: Advertisement): Observable<any> {
    return this.http.put(`/api/ads`, ad);
  }

  public selectRequest(req: Request): Observable<any> {
    return this.http.post(`/api/ads/${req.advertisement.id}/requests/${req.id}/SELECT`, null);
  }

  public cancelRequest(req: Request): Observable<any> {
    return this.http.post(`/api/ads/${req.advertisement.id}/requests/${req.id}/CANCEL`, null);
  }

  public changeStatus(ad: Advertisement, status: string): Observable<any> {
    return this.http.post(`/api/ads/${ad.id}/statuses/${status}`, null);
  }
}
