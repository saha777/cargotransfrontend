import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../shared/models/user';
import {isUndefined} from "util";
import {ContactsService} from "../../contacts/contacts.service";
import {AdvertisementsService} from "../advertisements.service";
import {Advertisement} from "../../shared/models/advertisement";

@Component({
  selector: 'app-ad-list',
  templateUrl: './ad-list.component.html',
  styleUrls: ['./ad-list.component.css']
})
export class AdListComponent implements OnInit {
  @Input() user: User;
  ads: Advertisement[] = [];

  constructor(private advertisementsService: AdvertisementsService) { }

  ngOnInit() {
    this.loadUserContacts();
  }

  loadUserContacts() {
    if (!isUndefined(this.user)) {
      this.advertisementsService.getByUserId(this.user.id).subscribe(
        contactsData => {
          this.ads = contactsData;
        }, error2 => {
          console.error(error2);
        }
      );
    }
  }
}
