import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {CreateAdvertisementComponent} from './advertisements/create-advertisement/create-advertisement.component';
import {AdvertisementsComponent} from './advertisements/advertisements/advertisements.component';
import {UserPageComponent} from './users/user-page/user-page.component';
import {LoginComponent} from './auth/login/login.component';
import {RegistrationComponent} from './auth/registration/registration.component';
import {AuthGuard} from './shared/services/auth-guard.service';
import {AdvertisementPageComponent} from './advertisements/advertisement-page/advertisement-page.component';
import {AdminRules} from './shared/services/admin-rules.service';
import {CarMakesListComponent} from './cars/makes/car-makes-list/car-makes-list.component';
import {CarMakesCreateComponent} from './cars/makes/car-makes-create/car-makes-create.component';
import {CarMakesUpdateComponent} from './cars/makes/car-makes-update/car-makes-update.component';
import {CarMakesDetailsComponent} from './cars/makes/car-makes-details/car-makes-details.component';
import {CarModelsListComponent} from './cars/models/car-models-list/car-models-list.component';
import {CarModelsCreateComponent} from './cars/models/car-models-create/car-models-create.component';
import {CarModelsUpdateComponent} from './cars/models/car-models-update/car-models-update.component';
import {CarModelsDetailsComponent} from './cars/models/car-models-details/car-models-details.component';
import {UpdateAdComponent} from './advertisements/update-ad/update-ad.component';
// import {AdvertisementPageComponent} from './advertisements/advertisement-page/advertisement-page.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'singUp', component: RegistrationComponent},
  {path: 'ads/create', component: CreateAdvertisementComponent, canActivate: [AuthGuard]},
  {path: 'ads/update/:id', component: UpdateAdComponent, canActivate: [AuthGuard]},
  {path: 'myPage', component: UserPageComponent, canActivate: [AuthGuard]},
  {path: 'ads/ad/:id', component: AdvertisementPageComponent},
  {path: 'ads', component: AdvertisementsComponent, canActivate: [AuthGuard]},
  {path: 'cars/makes', component: CarMakesListComponent, canActivate: [AdminRules]},
  {path: 'cars/makes/add', component: CarMakesCreateComponent, canActivate: [AdminRules]},
  {path: 'cars/makes/update/:id', component: CarMakesUpdateComponent, canActivate: [AdminRules]},
  {path: 'cars/makes/details/:id', component: CarMakesDetailsComponent, canActivate: [AdminRules]},
  {path: 'cars/models', component: CarModelsListComponent, canActivate: [AdminRules]},
  {path: 'cars/models/add', component: CarModelsCreateComponent, canActivate: [AdminRules]},
  {path: 'cars/models/update/:id', component: CarModelsUpdateComponent, canActivate: [AdminRules]},
  {path: 'cars/models/details/:id', component: CarModelsDetailsComponent, canActivate: [AdminRules]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
