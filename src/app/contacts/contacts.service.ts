import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {Contact} from '../shared/models/contact';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }

  getByUserId(userId: string): Observable<any> {
    return this.http.get(`/api/users/${userId}/contacts`);
  }

  addContact(userId: string, contact: Contact): Observable<any> {
    return this.http.post(`/api/users/${userId}/contacts`, contact);
  }

  updateContact(userId: string, contact: Contact): Observable<any> {
    return this.http.put(`/api/users/${userId}/contacts/${contact.id}`, contact);
  }

  deleteContact(userId: string, contactId: string): Observable<any> {
    return this.http.delete(`/api/users/${userId}/contacts/${contactId}`);
  }
}
