import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../shared/models/user";
import {isUndefined} from "util";
import {ContactsService} from "../contacts.service";
import {Contact} from "../../shared/models/contact";

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  @Input() user: User;
  @Output() contactsUpdate = new EventEmitter<Contact[]>();
  contacts: Contact[] = [];
  creatableContact: Contact = new Contact();
  updatableContact: Contact;

  constructor(private contactsService: ContactsService) { }

  ngOnInit() {
    console.log(this.user);
    this.loadUserContacts();
  }

  loadUserContacts() {
    if (!isUndefined(this.user)) {
      this.contactsService.getByUserId(this.user.id).subscribe(
        contactsData => {
          this.contacts = contactsData;
        }, error2 => {
          console.error(error2);
        }
      )
    }
  }

  addContact() {
    if (!isUndefined(this.user)) {
      this.contactsService.addContact(this.user.id, this.creatableContact).subscribe(
        contactData => {
          this.contacts.push(contactData);
          this.contactsUpdate.emit(this.contacts);
          this.creatableContact = new Contact();
        }, error2 => {
          console.error(error2);
        }
      )
    }
  }

  submitUpdatedContact() {
    if (!isUndefined(this.user)) {
      // this.contactsService.updateContact(this.user.id, this.updatableContact).subscribe(
      //   data => {
      //
      //   }, error => {
      //     console.error(error);
      //   }
      // );
    } else {

    }
  }

  submitDeletedContact(i: number) {
    // if (isUndefined(this.user)) {
      this.contactsService.deleteContact(this.user.id, this.contacts[i].id).subscribe(
        contactData => {
          this.contacts.splice(i, 1);
        }, error2 => {
          console.error(error2);
        }
      )
    // }
  }
}
