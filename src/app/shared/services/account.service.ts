import { Injectable } from '@angular/core';
import {User} from '../models/user';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {NavbarComponent} from '../../navbar/navbar.component';

@Injectable()
export class AccountService {

  constructor(private router: Router, private http: HttpClient) {
  }

  public login(account: User, token: string, roles: string[]) {
    localStorage.setItem('access_token', token);
    localStorage.setItem('current_user', JSON.stringify(account));
    localStorage.setItem('roles', JSON.stringify(roles));
    NavbarComponent.isLogined = true;
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('current_user');
    localStorage.removeItem('roles');
    NavbarComponent.isLogined = false;
    this.router.navigate(['']);
  }

  public isLogined(systemModule = true): boolean {
    const bool: boolean = (localStorage.access_token === null ||
      localStorage.access_token === undefined) ;
    console.log('isLogined: ' + !bool);
    NavbarComponent.isLogined = !bool;
    return !bool;
  }

  public hasAdminRole(): boolean {
    if (!this.isLogined()) {
      return false;
    }
    const roles: string[] = JSON.parse(localStorage.roles);
    for (const role of roles) {
      if (role === 'ROLE_ADMIN') {
        return true;
      }
    }
    return false;
  }

  public getUserAccount(): User {
    return JSON.parse(localStorage.current_user);
  }

  public getToken(): string  {
    return localStorage.access_token;
  }
}
