import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {UserCredentials} from '../models/user-credentials';
import {HttpClient} from '@angular/common/http';
import {RegisterModel} from '../../auth/registration/register.model';

@Injectable()
export class AppAuthService {

  constructor(private http: HttpClient) {
  }

  login(loginData: UserCredentials): Observable<any> {
    return this.http.post('/uaa/auth/login', loginData, {observe: 'response'});
  }

  // registration
  registration(account: RegisterModel): Observable<any> {
    return this.http.post('/uaa/auth/registration', account,{observe: 'response'});
  }
}
