import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {AccountService} from "./account.service";
import {Observable} from "rxjs/index";
import {tap} from "rxjs/internal/operators";

@Injectable()
export class TokenInterceptService implements HttpInterceptor {
  constructor(private accountService: AccountService) {
  }

  // intercept request and add token
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (this.accountService.isLogined()) {
      // modify request
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.accountService.getToken()
        }
      });

      console.log('Request: ');
      console.log(request);
    }


    return next.handle(request).pipe(tap(event => {
      if (event instanceof HttpResponse) {
        console.log(event.status);
      }
    }, error => {
      // http response status code
      // console.log('----response----');
      // console.error('status code:');
      console.error(error);
      console.error(error.status);
      if (error.status === 403 || error.status === 401) {
        this.accountService.logout();
      }
      console.error(error.message);
      // console.error(JSON.stringify(request.body));
      // console.log('--- end of response---');
    }));
  }
}
