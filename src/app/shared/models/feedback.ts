import {User} from "./user";
import {Advertisement} from "./advertisement";

export class Feedback {
  public id: string;
  public Rating: number;
  public Description: string;
  public User: User;
  public Advertisement: Advertisement;
}
