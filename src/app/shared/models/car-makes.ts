import {CarModel} from './car-model';

export class CarMakes {
  public id: number;
  public name: string;
  public carModels: CarModel[];
}
