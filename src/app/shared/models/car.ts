export class Car {
  public id: string;
  public makes: string;
  public model: string;
  public issueDate: Date;
  public maxCargoVolume: number;
  public maxCargoMass: number;
}
