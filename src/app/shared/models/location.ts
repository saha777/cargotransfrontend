export class LocationDto {
  public id: string;
  public name: string;
  public label: string;
  public latitude: number;
  public longitude: number;
}
