import {User} from './user';
import {LocationDto} from './location';
import {Request} from './request';

export class Advertisement {
  public id: string;
  public name: string;
  public startPoint: LocationDto;
  public endPoint: LocationDto;
  public orientedDateFrom: Date;
  public orientedDateTo: Date;
  public сreationDate: Date;
  public cargoVolume: number;
  public description: string;
  public owner: User;
  public ownerId: string;
  public status: string;
  public choosedRequest: Request;
  public choosedRequestId: string;
  public requests: Request[];
}
