import {Contact} from './contact';
import {Car} from './car';

export class User {
  public id: string;
  public name: string;
  public lastName: string;
  public email: string;
  public isDriver: boolean;
  public birthday: Date;
  public description: string;
  public contacts: Contact[];
  public cars: Car[];
}
