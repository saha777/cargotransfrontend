import {Advertisement} from './advertisement';
import {Car} from './car';
import {User} from './user';

export class Request {
  public id: string;
  public orientedCost: number;
  public description: string;
  public driver: User;
  public car: Car;
  public advertisementId: string;
  public advertisement: Advertisement;
}
