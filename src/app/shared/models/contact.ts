export class Contact {
  public id: string;
  public type: string;
  public name: string;
  public value: string;
}
