import {User} from '../../shared/models/user';

export class RegisterModel extends User {
  public password: string;
}
