import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../shared/services/account.service';
import {RegisterModel} from './register.model';
import {isUndefined, isNull} from 'util';
import {AppAuthService} from '../../shared/services/app-auth.service';
import {User} from '../../shared/models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  hasErrors = false;
  error: string;
  success: boolean = false;
  account: RegisterModel;
  confirmPassword: string;

  todayDate: Date;
  todayNumber: number;

  constructor(private accountService: AccountService,
              private appAuthService: AppAuthService,
              private router: Router) { }

  ngOnInit() {
    this.success = false;
    this.account = new RegisterModel();
    this.initDates();
    this.checkLogined();
  }

  checkLogined() {
    console.log('RegistrationComponentInit');
    console.log(this.accountService.isLogined());

    const bool: boolean = this.accountService.isLogined();

    if (bool) {
      this.router.navigate(['/ads']);
    }
  }

  initDates() {
    this.todayDate = this.getTodayString();
    this.todayNumber = this.todayDate.getTime();
  }

  private getTodayString(): Date {
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth();
    const yyyy = today.getFullYear();
    return new Date(yyyy, mm, dd);
  }

  isInvalidBirthday(): boolean {
    if (isUndefined(this.account.birthday) || isNull(this.account.birthday)) {
      return false;
    }
    return this.todayDate.getFullYear() - 18 < this.account.birthday.getFullYear();
  }

  setBirthday(value) {
    this.account.birthday = new Date(value);
  }

  registrate() {
    console.log(this.account);
    this.appAuthService.registration(this.account).subscribe(
      (res) => {
        this.successLogin(res);
      }, error => {
        this.showError(error);
      }
    );
  }

  successLogin(res) {
    console.log(res);
    const user: User = res.body.user;
    const roles: string[] = res.body.roles;
    this.accountService.login(user, res.body.token.access_token, roles);
    this.router.navigate(['/ads']);
  }

  showError(error) {
    console.log(error);
    this.success = null;
  }
}
