import { Component, OnInit } from '@angular/core';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider,
  VkontakteLoginProvider
} from 'angular-6-social-login-v2';
import {User} from '../../shared/models/user';
import {Router} from '@angular/router';
import {AccountService} from '../../shared/services/account.service';
import {UserCredentials} from '../../shared/models/user-credentials';
import {AppAuthService} from '../../shared/services/app-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  success: boolean;
  errorMessage: string;
  account: UserCredentials;

  constructor(private socialAuthService: AuthService,
              private appAuthService: AppAuthService,
              private accountService: AccountService,
              // private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    console.log('LoginComponent init');
    if (this.accountService.isLogined()) {
      this.router.navigate(['/ads']);
    }
    this.success = false;
    this.account = new UserCredentials();
  }



  logIn() {
    console.log(this.account);
    this.appAuthService.login(this.account).subscribe(
      (res) => {
        this.successLogin(res);
      }, error => {
        this.showError(error);
      }
    );
  }

  successLogin(res) {
    console.log(res);
    const user: User = res.body.user;
    const roles: string[] = res.body.roles;
    this.accountService.login(user, res.body.token.access_token, roles);
    this.router.navigate(['/ads']);
  }

  showError(error) {
    console.log(error);
    this.success = null;
    this.errorMessage = error.message;
  }
}
