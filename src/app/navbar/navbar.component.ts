import { Component, OnInit } from '@angular/core';
import {UsersService} from '../users/users.service';
import {User} from '../shared/models/user';
import {AccountService} from "../shared/services/account.service";
import {AdminRules} from '../shared/services/admin-rules.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  static isLogined: boolean = true;

  constructor(protected accountService: AccountService) {
  }

  ngOnInit() {
    console.log('HeaderComponent init');
    this.checkLogIn();
  }

  logout() {
    this.accountService.logout();
  }

  getLoginStatus(): boolean {
    return NavbarComponent.isLogined;
  }

  checkLogIn() {
    NavbarComponent.isLogined = this.accountService.isLogined();
  }

  getUserName(): string {
    return this.accountService.getUserAccount().name;
  }

  isDriver(): boolean {
    return this.accountService.getUserAccount().isDriver;
  }
}
