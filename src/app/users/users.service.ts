import {Injectable} from '@angular/core';
import {User} from '../shared/models/user';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UsersService {
  private static users: User[] = [];
  constructor(private http: HttpClient) {
  }

  public getById(id: string): Observable<any> {
    return this.http.get(`/api/users/${id}`);
  }
}
