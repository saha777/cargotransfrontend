import {Component, OnInit} from '@angular/core';
import {AccountService} from '../../shared/services/account.service';
import {User} from '../../shared/models/user';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {
  user: User;

  constructor(private accountService: AccountService) {
  }

  ngOnInit() {
    this.user = this.accountService.getUserAccount();
  }

}
