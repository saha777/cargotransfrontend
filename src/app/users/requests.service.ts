import {Injectable} from '@angular/core';
import {Request} from '../shared/models/request';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class RequestsService {

  constructor(private http: HttpClient) {
  }

  public getByAdId(id: string): Observable<any> {
    return this.http.get(`/api/requests/ads/${id}`);
  }

  public add(request: Request) {
    return this.http.post(`/api/requests`, request);
  }

  public update(request: Request) {
    return this.http.put(`/api/requests`, request);
  }

  public delete(requestId: string) {
    return this.http.delete(`/api/requests/${requestId}`);
  }
}
