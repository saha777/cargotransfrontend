import {Component, Input, OnInit} from '@angular/core';
import {isUndefined} from 'util';
import {RequestsService} from '../requests.service';
import {Advertisement} from '../../shared/models/advertisement';
import {User} from '../../shared/models/user';
import {Request} from '../../shared/models/request';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.css']
})
export class RequestsListComponent implements OnInit {

  @Input() driver: User;
  requests: Request[] = [];

  constructor(private requestsService: RequestsService) { }

  ngOnInit() {
    this.loadUserContacts();
  }

  loadUserContacts() {
    if (!isUndefined(this.driver)) {
      this.requestsService.getAdvertisementsByUserId(this.driver.id).subscribe(
        contactsData => {
          this.requests = contactsData;
          console.log(contactsData);
        }, error2 => {
          console.error(error2);
        }
      );
    }
  }
}
