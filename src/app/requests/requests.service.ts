import { Injectable } from '@angular/core';
import {Observable} from "rxjs/index";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: HttpClient) {}

  public getAdvertisementsByUserId(id: string): Observable<any> {
    return this.http.get(`/api/requests/users/${id}`);
    // console.log(AdvertisementsService.ads);
    // return AdvertisementsService.ads;
  }
}
